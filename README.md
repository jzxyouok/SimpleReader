#SimpleReader 简单阅读

使用到的第三方库：  

    compile 'com.getbase:floatingactionbutton:1.10.1'
    compile 'com.android.support:appcompat-v7:23.2.0'
    compile 'com.android.support:recyclerview-v7:23.2.0'
    compile 'com.android.support:cardview-v7:23.2.0'
    compile 'com.android.support:design:23.2.0'
    compile 'com.jakewharton:butterknife:6.1.0'
    compile 'com.jenzz:materialpreference:1.3'
    compile 'de.hdodenhof:circleimageview:1.3.0'
    compile 'com.pnikosis:materialish-progress:1.5'
    compile files('libs/volley.jar')

本项目界面设计参照于[http://git.oschina.net/Mr.LiaBin/my-oscgit-android](http://git.oschina.net/Mr.LiaBin/my-oscgit-android)

界面截图:  
![截图1](http://git.oschina.net/uploads/images/2016/0620/224606_259fa2d7_24648.png "截图1")
![截图2](http://git.oschina.net/uploads/images/2016/0620/224625_4d7444a8_24648.png "截图2")
![截图3](http://git.oschina.net/uploads/images/2016/0620/224640_edafc6ac_24648.png "截图3")
![截图4](http://git.oschina.net/uploads/images/2016/0620/224652_598088b8_24648.png "截图4")
![截图5](http://git.oschina.net/uploads/images/2016/0620/224706_5a258957_24648.png "截图5")
